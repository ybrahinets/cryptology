import re
from utils import modinv, extended_gcd, dec_to_string, fast_pow, bit_to_string, decrypt, isqrt

__author__ = "Yarik"

alphabet_en = "ABCDEFGHIJKLMNOPORSTUVWXYZ"
alphabet_ua = "АБВГГДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩ"


def split_number_to_char(string, alphabet):
    codes = re.findall('\d\d\d\d', string)
    blocks = ""
    try:
        for code in codes:
            blocks += alphabet[int(code[0:2])] + alphabet[int(code[2:4])] + ' '
    except IndexError:
        blocks = ''
        pass
    return blocks


def root_by_module(x, p):
    y = 0
    x %= p
    if (p - 3) % 4 == 0:  # p = 4m + 3
        m = int((p - 3) / 4)
        y = pow(x, m + 1, p)
    else:
        if (p - 5) % 8 == 0:  # p = 8m + 5
            m = int((p - 5) / 8)
            if pow(x, 2 * m + 1, p) == 1:
                y = pow(x, m + 1)
            else:
                y = pow(x, m + 1) * pow(2, 2 * m + 1)
        else:  # p = 8m + 1
            pass

    return y % p


def root_by_module_complex(x, p, q):
    x1 = x % p
    x2 = x % q
    a1 = root_by_module(x1, p)
    a2 = root_by_module(x2, q)
    gcd = extended_gcd(p, q)
    u, v = gcd[1], gcd[2]
    pairs = [[a1, a2], [-a1, a2], [a1, -a2], [-a1, -a2]]
    x = []
    for pair in pairs:
        x.append((pair[1] * p * u + pair[0] * q * v) % (p * q))

    return x


def rsa_dec(p, q, e, enc):
    phi = (p - 1) * (q - 1)
    d = modinv(e, phi)
    enc = enc.split(" ")
    dec = [pow(int(val), d, p * q) for val in enc]
    return dec


def rabin_dec(p, q, enc):
    enc = enc.split(" ")
    dec_modules = [root_by_module_complex(int(val), p, q) for val in enc]
    dec_modules_unique = [set(dec_module) for dec_module in dec_modules]

    dec_modules_aligned = []
    for dec_module in dec_modules_unique:
        for module in dec_module:
            module = int(module)
            dec_modules_aligned.append((4 - len(str(int(module)))) * '0' + str(module))
    dec = []
    for module in dec_modules_aligned:
        if len(split_number_to_char(module, alphabet_en)) != 0:
            dec.append(module)
    return dec


def elgamal_dec(p, q, a, enc):
    dec = []
    for pair in enc:
        num = (pair[1] * modinv(pow(pair[0], a), p)) % p
        c = str(num)
        if len(str(num)) != 4:
            c = (4 - len(c)) * '0' + c
        dec.append(c)
    return dec


def primitive_root(p):
    roots = []
    for n in range(1, p):
        group = set([])
        for power in range(0, p - 1):
            group.add(pow(n, power, p))
        if len(group) == p - 1:
            roots.append(n)
    return roots


def expand_into_factors(number):
    t = int(isqrt(number['n'])) + 1
    d = isqrt(pow(t, 2) - number['n'])
    number['p'] = int(t - d)
    number['q'] = int(t + d)


def main3_1():
    numbers = [
        {'n': 3029400079, 'p': None, 'q': None},
        {'n': 3029511281, 'p': None, 'q': None},
        {'n': 3028190057, 'p': None, 'q': None},
        {'n': 3032152289, 'p': None, 'q': None},
        {'n': 3029510657, 'p': None, 'q': None},
        {'n': 3028961071, 'p': None, 'q': None},
        {'n': 3031052989, 'p': None, 'q': None},
        {'n': 3030832793, 'p': None, 'q': None},
        {'n': 3028190057, 'p': None, 'q': None},
        {'n': 3032043871, 'p': None, 'q': None}
    ]

    print("\nTask 1 : Expand into factors")
    for idx, val in enumerate(numbers):
        expand_into_factors(val)
        print('\t[%d] : %d = %d * %d' % (idx, val['n'], val['p'], val['q']))


def main3_2():
    print("\nTask 2 : Decrypt RSA")

    dec = rsa_dec(47, 71, 19, '3011 1211')
    dec_str = split_number_to_char(''.join(str(x) for x in dec), alphabet_ua)
    print('\tDecrypted At classroom: ', dec_str)

    dec_h = rsa_dec(53, 61, 23, '0366 1226 0255')
    dec_str_h = split_number_to_char(''.join(str(x) for x in dec_h), alphabet_en)
    print('\tDecrypted At Home: ', dec_str_h)


def main3_3():
    # x = y^2 mod p
    print("\nTask 3 : Square root by modulus")
    p = 11
    q = 19
    numbers = [201, 125, 191, 159, 23, 80, 188, 111, 119, 92]
    for idx, val in enumerate(numbers):
        h = root_by_module_complex(val, 11, 19)
        print('\t[%d] : %d = [%s]^2 mod (%d)' % (idx, val, ' '.join(map(lambda e: str(int(e)), h)), p * q))


def main3_4():
    print("\nTask 4 : Rabin system")
    dec_h = rabin_dec(53, 67, '1497')
    dec_str_h = split_number_to_char(''.join(dec_h), alphabet_ua)
    print('\tDecrypted From book: ', dec_h, dec_str_h)

    dec_h = rabin_dec(67, 71, '2167 2351')
    dec_str_h = split_number_to_char(''.join(dec_h), alphabet_en)
    print('\tDecrypted At Home: ', dec_h, dec_str_h)


def main3_5():
    print("\nTask 5 : Primitive roots")
    numbers = [11, 13]
    # count of roots = phi(n-1), if n is prime
    for idx, val in enumerate(numbers):
        h = primitive_root(val)
        print('\t[%d] : %d = %s' % (idx, val, ' '.join(map(lambda e: str(int(e)), h))))


def main3_6():
    print("\nTask 6 : Decrypt Elgamal")
    dec_h = elgamal_dec(4519, 37, 23, [[1062, 814], [3034, 1172]])
    dec_str_h = split_number_to_char(''.join(str(x) for x in dec_h), alphabet_en)
    print('\tDecrypted At Home: ', dec_h, dec_str_h)


def main3_7():
    print("\nTask 7 : Diffie–Hellman")
    p = 4715958727385315387660737
    q = 5
    a = 10611587
    b = 31081541
    key = fast_pow(q, a * b, p)
    print("\tp = %d" % p)
    print("\ta = %d \tb = %d \tq = %d" % (a, b, q))
    print("\tkey = (q^a)^b mod p = (q^b)^a mod p = ", key)


def main3_8():
    n = 2155521117027243630973823963847695675142648931978141801990016117470096232504563101608940380959903957
    number = {'n': n, 'p': None, 'q': None}
    print("\nTask 8 : Expand into factors")
    expand_into_factors(number)
    print('\t%d = \n\t%d * %d' % (number['n'], number['p'], number['q']))


def main3_9():
    print("\nTask 9 : Decrypt RSA")
    c = '1369617520343736604681509545987401107472610213155449180224072143018640110640406921298600588205501186'
    n = 2155521117027243630973823963847695675142648931978141801990016117470096232504563101608940380959903957
    e = 65537
    number = {'n': n, 'p': None, 'q': None}
    expand_into_factors(number)
    dec = rsa_dec(number['p'], number['q'], e, c)
    print('\tDecrypted At Home: ', dec, dec_to_string(dec[0]))


def main3_10():
    print("\nTask 10 : Diffie–Hellman")
    p = 4715958727385315387660737
    q_b = 3193533191571883174254626
    enc = '101001101001000010110010000001000001010100000110000101101111101001011011'

    # texts = []
    # for i in range(1, 10000000):
    # key = pow(q_b, i, p)
    # text = decrypt(enc, key)
    # if is_text(text):
    # texts.append([text, i])

    a = 6082679  # matched a by brute force
    key = fast_pow(q_b, a, p)  # key = q ^ ab mod p
    dec = decrypt(enc, key)
    print("\tkey = (q^a)^b mod p = (q^b)^a mod p = ", key)
    print('\tEncrypted : ', bit_to_string('101001101001000010110010000001000001010100000110000101101111101001011011'))
    print('\tDecrypted : ', dec)


main3_1()
main3_2()
main3_3()
main3_4()
main3_5()
main3_6()
main3_7()
main3_8()
main3_9()
main3_10()