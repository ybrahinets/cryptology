__author__ = 'Yarik'


def extended_gcd(aa, bb):
    lastremainder, remainder = abs(aa), abs(bb)
    x, lastx, y, lasty = 0, 1, 1, 0
    while remainder:
        lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
        x, lastx = lastx - quotient * x, x
        y, lasty = lasty - quotient * y, y
    return lastremainder, lastx * (-1 if aa < 0 else 1), lasty * (-1 if bb < 0 else 1)


def modinv(a, m):
    g, x, y = extended_gcd(a, m)
    if g != 1:
        raise ValueError
    return x % m


def bit_to_string(bs):
    assert len(bs) % 8 == 0
    s = ""
    for i in range(0, len(bs), 8):
        s += chr(int(bs[i:i + 8], 2))
    return s


def string_to_bin(s):
    bs = ""
    for i in range(len(s)):
        bs += char_to_8bit(s[i])
    return bs


def xor_bits(bs1, bs2):
    bs = ""
    if len(bs2) < len(bs1):
        bs2 = (len(bs1) - len(bs2)) * '0' + bs2

    for i in range(len(bs1)):
        if bs1[i] == bs2[i]:
            bs += "0"
        else:
            bs += "1"
    return bs


def matmult(x, y):
    return [[sum(a * b for a, b in zip(x_row, y_col)) for y_col in zip(*y)] for x_row in x]


def dec_to_bin(x):
    return bin(x)[2:]


def char_to_8bit(c):
    # Переводить символ у 8-бітний бінарний рядок (ASCII)
    return bin(ord(c))[2:].zfill(8)


def dec_to_string(ds):
    # Переводить десяткове число у текстовий рядок
    bs = bin(ds)[2:]
    bs = bs.zfill(len(bs) + (8 - (len(bs) % 8)))
    s = ''
    for i in range(0, len(bs), 8):
        s += chr(int(bs[i:i + 8], 2))
    return s


def string_to_dec(s):
    bs = ''
    # Переводить текстовий рядок у десяткове число
    for i in range(len(s)):
        bs += char_to_8bit(s[i])
    return int(bs, 2)


def fast_pow(number, power, modulus):
    # a^b mod n
    b_2_reversed = reversed(dec_to_bin(power))  # reverse bits sequence of p

    res = 1
    for bit in b_2_reversed:
        if int(bit) & 1:
            res = res * number % modulus
        number = number * number % modulus
    return res


def is_text(text):
    # Перевіряє чи не містить даний рядок заборонених символів,
    # тобто чи не містить рядок символів крім літер англ. алфавіту, цифр і пробілу
    import string

    valid_chars = set(c for c in string.printable[:62])
    valid_chars.add(' ')
    return all(d in valid_chars for d in text)


def encrypt(plaintext, key):
    # Функція шифрування повідомлення; plaintext – текст; key – десяткове число.
    return xor_bits(string_to_bin(plaintext), dec_to_bin(key))


def decrypt(ciphertext, key):
    # Функція дешифрування повідомлення; ciphertext – бінарний рядок; key – десяткове число.
    return bit_to_string(xor_bits(ciphertext, dec_to_bin(key)))


def isqrt(n):
    x = n
    y = (x + 1) // 2
    while y < x:
        x = y
        y = (x + n // x) // 2
    return x