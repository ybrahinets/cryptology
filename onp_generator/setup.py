from cx_Freeze import setup, Executable

executables = [
    Executable("one_time_pad.py",
               appendScriptToExe=True,
               appendScriptToLibrary=False,
               )
]

buildOptions = dict(create_shared_zip=False)

setup(name="hello",
      version="0.1",
      description="the typical 'Hello, world!' script",
      options=dict(build_exe=buildOptions),
      executables=executables,
      )
# from cx_Freeze import setup, Executable

# setup( name = "" , version = "0.1" , description = "" , executables = [Executable("")] , )

#from cx_Freeze import setup, Executable

#setup( name = "sqrt" , version = "0.1" , description = "testw" , executables = [Executable("sqrt.py")] , )
