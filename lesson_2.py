from collections import namedtuple
import string
from utils import modinv, bit_to_string, xor_bits, string_to_bin

__author__ = "Yarik"

Range = namedtuple('Range', ['s', 'e'])


def affine_cipher_enc(a, s, text, alf):
    t = text.lower()
    alphabet = alf.lower()
    alphabet_len = len(alf.lower())

    return "".join([alphabet[(alphabet.index(symbol) * a + s) % alphabet_len] for symbol in t])


def affine_cipher_dec(a, s, crypto, alf):
    c = crypto.lower()
    alphabet = alf.lower()
    alphabet_len = len(alf.lower())
    a_inverse = modinv(a, len(alphabet))

    return "".join(
        [alphabet[(a_inverse * alphabet.index(symbol) + (-a_inverse * s % alphabet_len) % alphabet_len) % alphabet_len]
         for symbol in c])


def main2_1():
    text = "ANALYTICMETHODS"
    text_enc = "HUHYLIRDJZIGFOX"
    alf = string.ascii_uppercase
    a = 11
    s = 7

    print("Task 1 : Monogamous affine")
    print("\ta : ", a, "  s : ", s)
    print("\tSource Message : ", text)
    enc = affine_cipher_enc(a, s, text, alf)
    print("\tEncrypted : ", enc)
    dec = affine_cipher_dec(a, s, text_enc, alf)
    print("\tDecrypted : ", dec)


def main2_2():
    k = "110111000100000000010000000110110100101101001011010110011100111100010001" \
        "110001111110011101011110001000110011101111000001011001000010100100111111"
    c = "100100010010111101110100011011100010011100101010001010111110111101110000" \
        "101101011000111000101010010010110101011010100100000100000100000001011100"
    print('\nTask 2 : One-time pad')
    print('\tKey : ', bit_to_string(k))
    print('\tEncrypted : ', bit_to_string(c))
    print('\tDecrypted : ', bit_to_string(xor_bits(k, c)))


def main2_3():
    def rep(word, rng, key_extract, key_apply):
        key_part = xor_bits(string_to_bin(word), string_to_bin(bit_to_string(key_extract)[rng.s:rng.e]))
        return bit_to_string(xor_bits(key_part, string_to_bin(bit_to_string(key_apply)[rng.s:rng.e])))

    alp = string.ascii_uppercase + ' '
    c1 = '0111110010110111101000001010000011100001111100111100011010011100011111100000001110110010101110001110' \
         '0100100111000100111100101001101111001110111010011011010000110100111101001111001010111111001001111011' \
         '0001000001011000100110010111001011111000000001010111101101110010000010000110111001000101001100101010' \
         '0000100000000000111111011010001110011001010101111110010011101000111000110010000010010101010010100101'
    c2 = '0110100111111111101001101110100011101101111101011101101011011001001101110000010011111010101111101110' \
         '0100100101110100010100101001101111101111010111011010010111100101110001011000011001001110100001110100' \
         '0101010100011001100001110011110111100011000101000010100000100110000101110110111001011101011110011011' \
         '0110100001110001101010000011011101011000100001111000010000001000111001100001010010000001100111100000'
    c3 = '0111001111110010101101011010110111110010101110101101100011001001011000110101000011111101101100011110' \
         '1100110100000100001101100111101010011111010011010111000100000100111101010010001010011111001101101110' \
         '0100001001010110100111010111001011100110000110010110100100100110010111110111001001001011001011001110' \
         '0101100100000001101010010100011101011001100001111111000001011101101001111101010011010001010111111100'
    letters = int(len(c3) / 8)
    ab = bit_to_string(xor_bits(c1, c2))
    ac = bit_to_string(xor_bits(c1, c3))
    bc = bit_to_string(xor_bits(c2, c3))
    print("\nTask 3 : One-time pad triple key usage")
    print('\tab -> \t', ''.join([['-', s][s in alp] for s in ab]))
    print('\tac -> \t', ''.join([['-', s][s in alp] for s in ac]))
    print('\tbc -> \t', ''.join([['-', s][s in alp] for s in bc]))

    a_dec_word = ['_' for _ in range(letters)]
    b_dec_word = ['_' for _ in range(letters)]
    c_dec_word = ['_' for _ in range(letters)]

    for i in range(letters):
        if ab[i] in alp and ac[i] in alp:
            if ac[i] == ab[i]:
                a_dec_word[i] = '*'
                b_dec_word[i] = '*'
                c_dec_word[i] = '*'
            else:
                a_dec_word[i] = ' '
                b_dec_word[i] = ab[i].lower()
                c_dec_word[i] = ac[i].lower()
        elif ab[i] in alp and bc[i] in alp:
            if bc[i] == ab[i]:
                a_dec_word[i] = '*'
                b_dec_word[i] = '*'
                c_dec_word[i] = '*'
            else:
                a_dec_word[i] = ab[i].lower()
                b_dec_word[i] = ' '
                c_dec_word[i] = bc[i].lower()
        elif ac[i] in alp and bc[i] in alp:
            if bc[i] == ac[i]:
                a_dec_word[i] = '*'
                b_dec_word[i] = '*'
                c_dec_word[i] = '*'
            else:
                a_dec_word[i] = ac[i].lower()
                b_dec_word[i] = bc[i].lower()
                c_dec_word[i] = ' '

    # guess from 2nd row
    w = 'thINgS'
    r = Range(9, 15)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c2, c1), w, rep(w, r, c2, c3)

    # guess from 3rd row
    w = 'YOu'
    r = Range(34, 37)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w,

    # guess from 3rd row
    w = 'CAn'
    r = Range(38, 41)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w,

    # guess from 2rd row
    w = 'They'
    r = Range(32, 36)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c2, c1), w, rep(w, r, c2, c3)

    # guess from 3rd row
    w = 'Put'
    r = Range(6, 9)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w

    # guess from 3rd row
    w = 'Today'
    r = Range(45, 50)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w

    # guess from 2rd row
    w = 'More'
    r = Range(4, 8)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c2, c1), w, rep(w, r, c2, c3)

    # guess from 2rd row
    w = 'THe'
    r = Range(42, 45)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c2, c1), w, rep(w, r, c2, c3)

    # guess from 1rd row
    w = 'Link'
    r = Range(41, 45)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = w, rep(w, r, c1, c2), rep(w, r, c1, c3)

    # guess from 1rd row
    w = 'Chain'
    r = Range(2, 7)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = w, rep(w, r, c1, c2), rep(w, r, c1, c3)

    # guess from 2rd row
    w = 'The'
    r = Range(0, 3)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c2, c1), w, rep(w, r, c2, c3)

    # guess from 3rd row
    w = 'tomorrow'
    r = Range(20, 28)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w

    # guess from 3rd row
    w = 'until'
    r = Range(14, 19)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w

    # guess from 3rd row
    w = ' what'
    r = Range(28, 33)
    a_dec_word[r.s:r.e], b_dec_word[r.s:r.e], c_dec_word[r.s:r.e] = rep(w, r, c3, c1), rep(w, r, c3, c2), w

    print('\n\tc1 dec :\t ', ''.join(a_dec_word))
    print('\tc2 dec :\t ', ''.join(b_dec_word))
    print('\tc3 dec :\t ', ''.join(c_dec_word))

    key = xor_bits(string_to_bin(''.join(a_dec_word).lower()), c1)
    c1_dec = xor_bits(key, c1)
    c2_dec = xor_bits(key, c2)
    c3_dec = xor_bits(key, c3)

    print('\n\tK \t: \t', bit_to_string(key))

    # a chain is only as strong as its weakest link, Міцність ланцюга визначається міцністю найслабшої ланки
    print('\tC1 \t: \t', bit_to_string(c1), ' ->\t ', bit_to_string(c1_dec), '\tEQUALS : ',
          xor_bits(key, c1_dec).__eq__(c1))

    # the more things change the more they stay the same, Чим більше змін, тим більше вони залишаються тими ж
    print('\tC2 \t: \t', bit_to_string(c2), ' ->\t ', bit_to_string(c2_dec), '\tEQUALS : ',
          xor_bits(key, c2_dec).__eq__(c2))

    # never put off until tomorrow what you can do today, Ніколи не відкладай на завтра те, що можна зробити сьогодні
    print('\tC3 \t: \t', bit_to_string(c3), ' ->\t ', bit_to_string(c3_dec), '\tEQUALS : ',
          xor_bits(key, c3_dec).__eq__(c3))


def main2_4():
    alp = string.ascii_uppercase + ' '
    print("\nTask 4 : One-time pad seven key usage")
    # BB3A65F6F0034FA957F6A767699CE7FABA855AFB4F2B520AEAD612944A801E
    # BA7F24F2A35357A05CB8A16762C5A6AAAC924AE6447F0608A3D11388569A1E
    # A67261BBB30651BA5CF6BA297ED0E7B4E9894AA95E300247F0C0028F409A1E
    # A57261F5F0004BA74CF4AA2979D9A6B7AC854DA95E305203EC8515954C9D0F
    # BB3A70F3B91D48E84DF0AB702ECFEEB5BC8C5DA94C301E0BECD241954C831E
    # A6726DE8F01A50E849EDBC6C7C9CF2B2A88E19FD423E0647ECCB04DD4C9D1E
    # BC7570BBBF1D46E85AF9AA6C7A9CEFA9E9825CFD5E3A0047F7CD009305A71E
    # i am planning a secret mission
    # he is the only person to trust
    # the current plan is top secret
    # when should we meet to do this
    # i think they should follow him
    # this is purer than that one is
    # not one cadet is better than I

    c1 = bin(int('BB3A65F6F0034FA957F6A767699CE7FABA855AFB4F2B520AEAD612944A801E', 16))[2:].zfill(8)
    c2 = bin(int('BA7F24F2A35357A05CB8A16762C5A6AAAC924AE6447F0608A3D11388569A1E', 16))[2:].zfill(8)
    c3 = bin(int('A67261BBB30651BA5CF6BA297ED0E7B4E9894AA95E300247F0C0028F409A1E', 16))[2:].zfill(8)
    c4 = bin(int('A57261F5F0004BA74CF4AA2979D9A6B7AC854DA95E305203EC8515954C9D0F', 16))[2:].zfill(8)
    c5 = bin(int('BB3A70F3B91D48E84DF0AB702ECFEEB5BC8C5DA94C301E0BECD241954C831E', 16))[2:].zfill(8)
    c6 = bin(int('A6726DE8F01A50E849EDBC6C7C9CF2B2A88E19FD423E0647ECCB04DD4C9D1E', 16))[2:].zfill(8)
    c7 = bin(int('BC7570BBBF1D46E85AF9AA6C7A9CEFA9E9825CFD5E3A0047F7CD009305A71E', 16))[2:].zfill(8)

    c1_2 = bit_to_string(xor_bits(c1, c2))
    c1_3 = bit_to_string(xor_bits(c1, c3))
    c1_4 = bit_to_string(xor_bits(c1, c4))
    c1_5 = bit_to_string(xor_bits(c1, c5))
    c1_6 = bit_to_string(xor_bits(c1, c6))
    c1_7 = bit_to_string(xor_bits(c1, c7))
    print('\tc_1_2 -> \t', ''.join([['-', s][s in alp] for s in c1_2]))
    print('\tc_1_3 -> \t', ''.join([['-', s][s in alp] for s in c1_3]))
    print('\tc_1_4 -> \t', ''.join([['-', s][s in alp] for s in c1_4]))
    print('\tc_1_5 -> \t', ''.join([['-', s][s in alp] for s in c1_5]))
    print('\tc_1_6 -> \t', ''.join([['-', s][s in alp] for s in c1_6]))
    print('\tc_1_7 -> \t', ''.join([['-', s][s in alp] for s in c1_7]))

    key = xor_bits(string_to_bin('the current plan is top secret').lower(), c3)
    c1_dec = xor_bits(key, c1)
    c2_dec = xor_bits(key, c2)
    c3_dec = xor_bits(key, c3)
    c4_dec = xor_bits(key, c4)
    c5_dec = xor_bits(key, c5)
    c6_dec = xor_bits(key, c6)
    c7_dec = xor_bits(key, c7)
    print('\n\tkey -> \t\t', bit_to_string(key))
    print('\tc1 -> \t\t', bit_to_string(c1_dec))
    print('\tc2 -> \t\t', bit_to_string(c2_dec))
    print('\tc3 -> \t\t', bit_to_string(c3_dec))
    print('\tc4 -> \t\t', bit_to_string(c4_dec))
    print('\tc5 -> \t\t', bit_to_string(c5_dec))
    print('\tc6 -> \t\t', bit_to_string(c6_dec))
    print('\tc7 -> \t\t', bit_to_string(c7_dec))


main2_1()
main2_2()
main2_3()
main2_4()