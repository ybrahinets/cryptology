import re
import string
import math
import fractions
from utils import modinv, matmult

__author__ = 'Yarik'


def print_results(title, array):
    print('\n', title)
    for ind, val in enumerate(array):
        print('\t var ', ind, ' : ', val)


def four_square(encrypted, squares):
    sq = int(math.sqrt(len(squares[0])))
    res = ''
    for i in range(0, len(encrypted), 2):
        r_t = [int(squares[1].index(encrypted[i]) / sq), squares[1].index(encrypted[i]) % sq]
        l_b = [int(squares[2].index(encrypted[i + 1]) / sq), squares[2].index(encrypted[i + 1]) % sq]
        res += (squares[0][r_t[0] * sq + l_b[1]] + squares[3][l_b[0] * sq + r_t[1]])

    return res


def caesar(encrypted, key):
    return ''.join(map(lambda x: chr((ord(x) - ord('A') - key) % 26 + ord('A')), encrypted))


def vigenere(encrypted, key):
    return ''.join(chr((ord(val) - ord(key[i % len(key)])) % 26 + ord('A')) for i, val in enumerate(encrypted))


def permutation(encrypted, key):
    sub_arr = re.findall(int(len(encrypted) / len(key)) * '.', encrypted)
    array = [list(sub_arr[sorted(key).index(k)]) for k in key]
    res = ''.join((array[i % len(key)].pop(0)).__str__() for i in range(len(encrypted)))

    return res.rstrip('X')


def adfgvx(encrypted, key):
    algo = "ADFGVX"
    adfgvx_matrix = ['rxjo09',
                     'vheg5y',
                     'snwtli',
                     'dz13f7',
                     '4qpmab',
                     '28cu6k']

    if len(encrypted) % len(key) != 0:  # align encrypted word
        exp_max = int(len(encrypted) / len(key)) + 1
        exp_min = int(len(encrypted) / len(key))
        letters = key[:len(encrypted) % len(key) - 1:-1]
        encrypted_ = ''
        for l in sorted(key):
            if l in letters:
                count = exp_min
                encrypted_ += encrypted[:count] + 'X'
            else:
                count = exp_max
                encrypted_ += encrypted[:count]
            encrypted = encrypted[count:]
        encrypted = encrypted_

    encrypted = permutation(encrypted, key)
    res = [adfgvx_matrix[algo.index(encrypted[i])][algo.index(encrypted[i + 1])] for i in range(0, len(encrypted), 2)]
    return ''.join(res).upper()


def monogamous_affine(a, s, encrypted):
    alp = string.ascii_uppercase
    a_ = modinv(a, len(alp))

    return ''.join([alp[(a_ * alp.index(symbol) + (-a_ * s % 26) % 26) % 26] for symbol in encrypted])


def top_affine_1(matrix, encrypted):
    alp = string.ascii_uppercase
    [[a, b], [c, d]] = matrix
    w = a * d - b * c
    n = len(alp)
    matrix_inverse = [[d * modinv(w, n) % n, -b * modinv(w, n) % n], [-c * modinv(w, n) % n, a * modinv(w, n) % n]]
    crypto = [[alp.index(encrypted[i]) for i in range(0, len(encrypted), 2)],
              [alp.index(encrypted[i]) for i in range(1, len(encrypted), 2)]]
    result = matmult(matrix_inverse, crypto)
    res = [[alp[(e % 26)] for e in p] for p in result]
    res = ''.join([res[j][i] for i in range(int(len(encrypted) / 2)) for j in range(2)])
    return res


def top_affine_2(dec_part, enc_part, encrypted):
    alp = string.ascii_uppercase
    encrypted_mat = [[alp.index(encrypted[i]) for i in range(0, len(encrypted), 2)],
                     [alp.index(encrypted[i]) for i in range(1, len(encrypted), 2)]]
    dec_part_mat = [[alp.index(dec_part[i]) for i in range(0, len(dec_part), 2)],
                    [alp.index(dec_part[i]) for i in range(1, len(dec_part), 2)]]
    enc_part_mat = [[alp.index(enc_part[i]) for i in range(0, len(enc_part), 2)],
                    [alp.index(enc_part[i]) for i in range(1, len(enc_part), 2)]]
    modulus = len(alp)
    [[a, b], [c, d]] = enc_part_mat
    det = a * d - b * c
    if fractions.gcd(det, modulus) == 1:
        coefficient = modinv(det, modulus)
        matrix_inv = [[d * coefficient % modulus, -b * coefficient % modulus],
                      [-c * coefficient % modulus, a * coefficient % modulus]]
        matrix_inverse = [[c % modulus for c in row] for row in matmult(dec_part_mat, matrix_inv)]
    else:
        dec_part_mat = [[c % (modulus / 2) for c in row] for row in dec_part_mat]
        enc_part_mat = [[c % (modulus / 2) for c in row] for row in enc_part_mat]
        [[a, b], [c, d]] = enc_part_mat
        det = a * d - b * c
        coefficient = modinv(det, modulus / 2)
        matrix_inv_ = [[d * coefficient % (modulus / 2), -b * coefficient % (modulus / 2)],
                       [-c * coefficient % (modulus / 2), a * coefficient % (modulus / 2)]]
        matrix_inverse_ = [[c % (modulus / 2) for c in row] for row in matmult(dec_part_mat, matrix_inv_)]
        matrix_inverse__ = [[0, 0], [1, 0]]
        matrix_inverse = [
            [matrix_inverse_[i][j] + modulus * matrix_inverse__[i][j] / 2 for j in range(len(matrix_inverse_))]
            for i in range(len(matrix_inverse_))]

    result = matmult(matrix_inverse, encrypted_mat)
    res = [[alp[int(e) % modulus] for e in p] for p in result]
    res = ''.join([res[j][i] for i in range(int(len(encrypted) / 2)) for j in range(2)])
    return res


def lcg(a, c, m, x_0, n):
    assert n > 0
    pseudo_list = [x_0]
    for i in range(n - 1):
        x_0 = (a * x_0 + c) % m
        pseudo_list += [x_0]
    return pseudo_list


def lcg_task():
    # x[1] = a*x[0] + c
    # x[2] = a*x[1] + c
    # ->
    # x         a               b
    # a * (x[1] - x[0]) = (x[2] - x[1])
    # x = (b * modInv(a, m)) mod (m)

    x = [2147483769, 2175910232, 4134845115, 1318263442, 1999771405, 769052060, 3994265071]
    m = pow(2, 32)
    a = ((x[2] - x[1]) * modinv(x[1] - x[0], m)) % m
    c = (x[1] - a * x[0]) % m
    return lcg(a, c, m, x[0], 8)[7]


def main_1():
    square = [
        'abcdefghiklmnopqrstuvwxyz',
        'SUNAEGIYOKHWRQBLCTPFMZDXV',
        'IZDNSPLUBWVHEOFTRKAQXYCMG',
        'abcdefghiklmnopqrstuvwxyz'
    ]
    print_results("Task 1 : 4 Square",
                  [
                      four_square("SQLSPBRO", square),
                      four_square("NGCAUFCF", square),
                      four_square("SGSVLSFN", square),
                      four_square("KDQQUPRO", square),
                      four_square("YOFNLYSV", square),
                      four_square("YAWOAQPY", square),
                      four_square("OCQFTBNQYA", square),
                      four_square("QLQUQLVS", square),
                      four_square("UTOAIEAQYN", square),
                      four_square("PCWHAQPY", square)
                  ])


def main_2():
    print_results("Task 2 : Caesar",
                  [
                      caesar("SHULRGLFIXQFWLRQ", 3),
                      caesar("SBWKDJRUHDQWKHRUHP", 3),
                      caesar("TXDGUDWLFSROBQRPLDO", 3),
                      caesar("UHVWULFWHGIXQFWLRQ", 3),
                      caesar("VHFRQGGHULYDWLYH", 3),
                      caesar("VXEVWLWXWLRQPHWKRG", 3),
                      caesar("WUXQFDWHGSBUDPLG", 3),
                      caesar("XQFRXQWDEOBLQILQLWH", 3),
                      caesar("ZKROHQXPEHUV", 3),
                      caesar("CHURRIDIXQFWLRQ", 3)
                  ])


def main_3():
    print_results("Task 3 : Vigenere",
                  [
                      vigenere("MBLVEYKQXYVVGZVDSEGA", "MATHTERM"),
                      vigenere("MFYPGIKDMNLMHVDMFIHU", "MATHTERM"),
                      vigenere("ZOGPGZVDFIUSXQRFDIQ", "MATHTERM"),
                      vigenere("PIYMXVVZFITSXULMFIHU", "MATHTERM"),
                      vigenere("QXIVGIEFUAEMNRTFUOG", "MATHTERM"),
                      vigenere("SEHTXXIUOPKVZVVEEIHU", "MATHTERM"),
                      vigenere("SRXHMIJFOOFTHRWMOTHY", "MATHTERM"),
                      vigenere("UNWLIIEPQNMCTVZMNLX", "MATHTERM"),
                      vigenere("XIGLTVIQSRXZLMFZ", "MATHTERM"),
                      vigenere("YOWBEEIMDIMOFIKUO", "MATHTERM")
                  ])


def main_4():
    print_results("Task 4 : Permutation",
                  [
                      permutation("AIIOXAETEXBFOEXNXNRXHPHXXCDTMX", "PERMUT"),
                      permutation("OWTHXZIAOXBOSTMLEREXNRSEXAESRX", "PERMUT"),
                      permutation("AANELIHXCHORYMTMYTOXELEX", "PERMUT"),
                      permutation("ELTMMTEXFSEERIHXTLRXATOX", "PERMUT"),
                      permutation("RREEPCHXGTRRAUTMSUOXHTEX", "PERMUT"),
                      permutation("ANLECEHXCIARUTTMYROXHGEX", "PERMUT"),
                      permutation("XRTMENEXEOEETAHXILRXRGOX", "PERMUT"),
                      permutation("RNIENOHXBIKRUKTMMSOXNWEX", "PERMUT"),
                      permutation("UNEFRDARLXFEHOBNTOAAMTMEXALEGX", "PERMUT"),
                      permutation("MTIOLUNEIITEPFORCCHXINTM", "PERMUT")
                  ])


def main_5():
    print_results("Task 5 : ADFGVX",
                  [
                      adfgvx("DADVFDDGDAFFXFGADGDDGFVADFVF", "GERMAN"),
                      adfgvx("FFDFFDGXXFDVFFXFVXDVVFDFDFVG", "GERMAN"),
                      adfgvx("FDVDAFAAAADFAFFAFGDFVFDGXF", "GERMAN"),
                      adfgvx("XVVFGAFFGDFVDAXFFVGFFDFDDV", "GERMAN"),
                      adfgvx("FVVFFFVGGGFFVVFXXDXGVVVVDFFDFA", "GERMAN"),
                      adfgvx("FDXDFFVAAADGAAGFGXADGFAVAF", "GERMAN"),
                      adfgvx("AVFFGXFFFVFDDDVDADAGADVFAF", "GERMAN"),
                      adfgvx("XADXGADAAXADGGFXVAFDFAFXAFVGDF", "GERMAN"),
                      adfgvx("ADAAFXVXAVXFFFVVFFGAAFAAAVFDFF", "GERMAN"),
                      adfgvx("VFFXVFGFVDAAFFDVDGFXFF", "GERMAN")
                  ])


def main_6():
    print_results("Task 6 : Monogamous affine",
                  [
                      monogamous_affine(11, 7, "HUHYLIRDJZIGFOX"),
                      monogamous_affine(11, 7, "HUVYZSRXZDIFM"),
                      monogamous_affine(17, 3, "LIHXTCJQOTGWDI"),
                      monogamous_affine(17, 3, "CTGJWDOJWT"),
                      monogamous_affine(7, 13, "IHIPBNKPICHA"),
                      monogamous_affine(7, 13, "PSOHAPAQRNQRHA"),
                      monogamous_affine(9, 5, "YFXUBCZFA"),
                      monogamous_affine(9, 5, "QNKBUPSDLP"),
                      monogamous_affine(5, 19, "HDLFTCNIALG"),
                      monogamous_affine(5, 19, "WLZNAYLPGI")
                  ])


def main_7():
    print_results("Task 7 : Top affine 1",
                  [
                      top_affine_1([[4, 7], [17, 3]], "ZHVKEBUUYI"),
                      top_affine_1([[2, 3], [7, 8]], "HKBVADKWKM"),
                      top_affine_1([[1, 5], [8, 11]], "DRCLBWSOGK"),
                      top_affine_1([[6, 1], [3, 17]], "LFHSULECYA"),
                      top_affine_1([[2, 3], [5, 11]], "HRBEABKMKA"),
                      top_affine_1([[12, 5], [1, 10]], "DGTTAVISIA"),
                      top_affine_1([[7, 3], [2, 15]], "HJEVJWWKSG"),
                      top_affine_1([[8, 13], [1, 3]], "NHXQETOSSY"),
                      top_affine_1([[6, 11], [7, 4]], "RSPXINEWMW"),
                      top_affine_1([[11, 2], [4, 5]], "WDDJHWQUKQ"),
                  ])


def main_8():
    print_results("Task 8 : Top affine 2", [top_affine_2("PHER", "LRCX", "FHYSYSZQZVAWLRCX")])


def main_9():
    print_results("Task 9 : Top affine 3", [top_affine_2("CISE", "MYSQ", "HLNCNMPCMYSQ")])


def main_10():
    print_results("Task 10 : LCG", [lcg_task()])


main_1()
main_2()
main_3()
main_4()
main_5()
main_6()
main_7()
main_8()
main_9()
main_10()